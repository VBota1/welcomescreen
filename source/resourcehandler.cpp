/*
MIT License

Copyright (c) 2018-2019 Academix Team (contact@academixproject.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Academix Team members:
Luta Dumitru - Project Lead
Cristi Rusu – Testing and translation
Stefan Keresztes – Testing and translation
Sima Elena Madalina – Testing and translation
Stan Valentin – Testing and translation
Abibula Aygun – Public realtions, testing and developement
Caius Gherle – Sponsor, web and testing
Giovanni Brancato – Testing and translation
Nicola Grilli - Graphics
Georgeta Poenaru – Testing and translation
Dicu Florin Alexandru – Testing and translation
Adrian Nicolae Stan - Public relations and testing
Anca Rădoi - testing
Viorel Bota - Testing and developement
Victor Ciornea - Testing
Silviu Petrache -Testing
David Raul Dobai -Testing
Valentin Stroe -Testing and translation
Mihai-Stanislav Jalobeanu - Mentor, Doctor in Mathematics,
                            Professor in informatics, Researcher

3rd Party Licenses:
This work is dynamically linked to the Qt libraries installed on the target device.
The Qt Libraries are covered by the following LGPL3 that can be viewed here:
https://gitlab.com/VBota1/welcomescreen/tree/master/3rdPartyLicenses

The Qt Libraryes includes multiple modules licensed under diferent conditions.
The link below contains a list of documents required by these licenses:
https://gitlab.com/VBota1/welcomescreen/tree/master/3rdPartyLicenses

The Qt Libraryes used in this software use components based in part on the work
of the Independent JPEG Group.

The Qt Libraryes used in this software use components based in part on the work
of the PNG Reference Library

The Qt Libraryes used in this software use components that are
copyright © 1996-2002, 2006; The FreeType Project (www.freetype.org).

The Qt Libraryes used in this software use components based in part on the work
that is copyright (C) 1994-1996, Thomas G. Lane. (see ffmpeg_License.txt)

The text of this license and the 3rd Party Licenses is also shipped
with the binary build of this application.
 */

#include "resourcehandler.h"

ResourceHandler::ResourceHandler()
{
    Logger log;
    qInstallMessageHandler(log.logMessage);

    if (QDir(resourceFolder).exists())
        isResourceFolderAvailalbe = true;
    else
        qCritical(QLoggingCategory("ResourceHandler::ResourceHandler")) << "Missing resource folder " << resourceFolder;
}


QUrl ResourceHandler::getUrlToShow() {
    if (homePage.isValid())
        return homePage;

    if (!isResourceFolderAvailalbe)
        return homePage;

    QString resourceFile = resourceFolder+folderDelimiter+homePageFile;
    QFileInfo check_file(resourceFile);
    if (check_file.exists() && check_file.isFile())
        homePage = QUrl("file:" + resourceFile);
    else
        qCritical(QLoggingCategory("ResourceHandler::getUrlToShow")) << "Missing resource file " << resourceFile;

    return homePage;
}

QUrl ResourceHandler::getUrlWithMoreInformation() {
    if (detailsPage.isValid())
        return detailsPage;

    getExternalLinks();

    return detailsPage;
}

QUrl ResourceHandler::getCommunityUrl() {
    if (comunityPage.isValid())
        return comunityPage;

    getExternalLinks();

    return comunityPage;
}

void ResourceHandler::getExternalLinks() {
    if (!isResourceFolderAvailalbe)
        return;

    QString resourceFileName = resourceFolder+folderDelimiter+moreInfoFile;
    QFileInfo check_file(resourceFileName);
    if (check_file.exists() && check_file.isFile()) {
        parseMoreInfoFile(resourceFileName);
    }
    else
        qCritical(QLoggingCategory("ResourceHandler::getExternalLinks")) << "Missing resource file " << resourceFileName;
}

void ResourceHandler::parseMoreInfoFile(QString resourceFileName) {
    const QString homePageToken = "home=";
    const QString communityPageToken = "comunity=";
    QFile resourceFile(resourceFileName);

    resourceFile.open(QFile::ReadOnly | QFile::Text);

    QString line;
    do {
        line = resourceFile.readLine();
        if (line.startsWith(homePageToken))
            detailsPage = QUrl(line.replace(homePageToken,"").trimmed());
        else if (line.startsWith(communityPageToken))
            comunityPage = QUrl(line.replace(communityPageToken,"").trimmed());
    }while( !line.isEmpty() && !line.isNull() );

    resourceFile.close();
}

QIcon ResourceHandler::getIcon() {
    if (!logo.isNull())
        return logo;

    if (!isResourceFolderAvailalbe)
        return logo;

    QString resourceFileName = resourceFolder+folderDelimiter+logoFile;
    QFileInfo check_file(resourceFileName);
    if (check_file.exists() && check_file.isFile())
        logo = QIcon(resourceFileName);
    else
        qCritical(QLoggingCategory("ResourceHandler::getIcon")) << "Missing resource file " << resourceFileName;

    return logo;
}

QString ResourceHandler::getLicense() {
    if (!license.isNull() && !license.isEmpty())
        return license;

    QString resourceFileName = resourceFolder+folderDelimiter+licenseFile;
    QFile resourceFile(resourceFileName);
    QFileInfo check_file(resourceFile);
    if (check_file.exists() && check_file.isFile()) {
        resourceFile.open(QFile::ReadOnly | QFile::Text);
        license = resourceFile.readAll().trimmed();
        resourceFile.close();
        license = applicationName + " Version: " + version + " " + license;
    }
    else
        qCritical(QLoggingCategory("ResourceHandler::getLicense")) << "Missing resource file " << resourceFileName;

    return license;
}

void ResourceHandler::doNotShowAgain() {
    if (!QDir(resourceFolder).exists())
        return;
    QFile inhibitor(QDir::homePath()+folderDelimiter+configFolder+folderDelimiter+inhibitorFile);
    inhibitor.open(QIODevice::WriteOnly);
    inhibitor.close();
}

bool ResourceHandler::userRequestedNotToShow() {
    QFileInfo check_file(QDir::homePath()+folderDelimiter+configFolder+folderDelimiter+inhibitorFile);
    if (check_file.exists() && check_file.isFile())
        return true;
    return false;
}
