/*
MIT License

Copyright (c) 2018-2019 Academix Team (contact@academixproject.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Academix Team members:
Luta Dumitru - Project Lead
Cristi Rusu – Testing and translation
Stefan Keresztes – Testing and translation
Sima Elena Madalina – Testing and translation
Stan Valentin – Testing and translation
Abibula Aygun – Public realtions, testing and developement
Caius Gherle – Sponsor, web and testing
Giovanni Brancato – Testing and translation
Nicola Grilli - Graphics
Georgeta Poenaru – Testing and translation
Dicu Florin Alexandru – Testing and translation
Adrian Nicolae Stan - Public relations and testing
Anca Rădoi - testing
Viorel Bota - Testing and developement
Victor Ciornea - Testing
Silviu Petrache -Testing
David Raul Dobai -Testing
Valentin Stroe -Testing and translation
Mihai-Stanislav Jalobeanu - Mentor, Doctor in Mathematics,
                            Professor in informatics, Researcher

3rd Party Licenses:
This work is dynamically linked to the Qt libraries installed on the target device.
The Qt Libraries are covered by the following LGPL3 that can be viewed here:
https://gitlab.com/VBota1/welcomescreen/tree/master/3rdPartyLicenses

The Qt Libraryes includes multiple modules licensed under diferent conditions.
The link below contains a list of documents required by these licenses:
https://gitlab.com/VBota1/welcomescreen/tree/master/3rdPartyLicenses

The Qt Libraryes used in this software use components based in part on the work
of the Independent JPEG Group.

The Qt Libraryes used in this software use components based in part on the work
of the PNG Reference Library

The Qt Libraryes used in this software use components that are
copyright © 1996-2002, 2006; The FreeType Project (www.freetype.org).

The Qt Libraryes used in this software use components based in part on the work
that is copyright (C) 1994-1996, Thomas G. Lane. (see ffmpeg_License.txt)

The text of this license and the 3rd Party Licenses is also shipped
with the binary build of this application.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    setupUi();
    configureAboutWindow(parent);
}

void MainWindow::setupUi()
{
    QIcon logo = resources.getIcon();
    if (!logo.isNull())
        this->setWindowIcon(logo);
    ui->setupUi(this);

    mainWindow = new QWebEngineView(this);
    mainWindow->resize(600,400);
    ui->mainLayout->insertWidget(0,mainWindow);
}

void MainWindow::configureAboutWindow(QWidget *parent)
{
    licenseViewer = new QTextBrowser(parent);
    QIcon logo = resources.getIcon();
    if (!logo.isNull())
        licenseViewer->setWindowIcon(logo);
    licenseViewer->setWindowTitle("About");
    licenseViewer->setAttribute(Qt::WA_QuitOnClose, false);
    licenseViewer->setMinimumWidth(600);
    licenseViewer->setMinimumHeight(400);
}

bool MainWindow::loadContent() {
    if (resources.userRequestedNotToShow())
        return false;

    QUrl content = resources.getUrlToShow();
    if (!content.isValid())
        return false;

    mainWindow->load(content);
    return true;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_about_clicked()
{
    showLicense();
}

void MainWindow::showLicense()
{
    QString license = resources.getLicense();
    if (license.isNull() || license.isEmpty())
        return;

    licenseViewer->setText(license);
    licenseViewer->show();
}

void MainWindow::on_cancel_clicked()
{
    resources.doNotShowAgain();
}

void MainWindow::on_moreInfo_clicked()
{
    QUrl content = resources.getUrlWithMoreInformation();
    if (!content.isValid())
        return;

    if (content.isValid())
        QDesktopServices::openUrl ( content );
}

void MainWindow::on_facebook_clicked()
{
    QUrl content = resources.getCommunityUrl();
    if (!content.isValid())
        return;

    if (content.isValid())
        QDesktopServices::openUrl ( content );
}
